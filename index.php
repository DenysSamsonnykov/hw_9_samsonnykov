<?php
session_start();

$currencies = [
    'uah' => [
       'name' => 'Гривна',
       'course' => 1,
    ],
    'usd' => [
       'name' => 'Доллар',
       'course' => 27.1,
    ],
    'eur' => [
       'name' => 'Евро',
       'course' => 30.2,
    ],
 ];

 $products = [
    ['title' => 'Хлеб',
     'price_val' => 20
    ],
    ['title' => 'Молоко',
     'price_val' => 34
    ],
    ['title' => 'Чипсы',
     'price_val' => 25
    ],
    ['title' => 'Вода б/г',
     'price_val' => 15
    ],
    ['title' => 'Петрушка',
     'price_val' => 10
    ],
    ['title' => 'Творог',
     'price_val' => 56
    ],
    ['title' => 'Авокадо',
     'price_val' => 45
    ],
    ['title' => 'Банан',
     'price_val' => 32
    ],
    ['title' => 'Яблоко',
     'price_val' => 19
    ],
    ['title' => 'Желе',
     'price_val' => 12
    ]
  ];
$value = $currencies['uah']; // если закоментировать эту строчку, код все ровно продолжает правильно работать, почему? $value же в таком случае не задана
if(!empty($_SESSION['currencies'])){
    $value = $currencies[$_SESSION['currencies']];
};

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <meta name="Description" content="learning $_SESSION">
    <title>Let's do this SESSION </title>
</head>
<body>
    <h1>Магазин "КООПЕРАТИВ"</h1>
    <h2>Для отображения актуальных цен выберете подходящую валюту</h2>
    <form action="logics.php" method="post" class="float-left">
        <div class="form-group mb-3">
            <select class="form-select" name="currencies">
                <option value="uah" selected>Выберите валюту</option> 
                <option value="uah"><?= $currencies['uah']['name']; ?></option>
                <option value="usd"><?= $currencies['usd']['name']; ?></option>
                <option value="eur"><?= $currencies['eur']['name']; ?></option>
            </select>
        </div>
        <button class="btn btn-warning">Посчитать</button>
    </form>  
    <br/>  
    <h2>Вами выбрана валюта: <?= $value['name']; ?> </h2>
    <p>И все цены в таблице указаны соответственно</p>
    <div class="float-left">
        <table class="table table-warning table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название товара</th>
                    <th scope="col">Цена товара</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($products as $key => $product) : ?>   
                    <tr>
                        <th scope="row"><?= ++$key; ?></th> 
                        <td><?= $product['title']; ?></td>
                        <td><?= round($product['price_val'] / $value['course'], 2); ?></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
    </div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script> 
</body>
</html>